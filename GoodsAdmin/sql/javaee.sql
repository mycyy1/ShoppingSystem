/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : javaee

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 24/12/2021 16:13:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for advert
-- ----------------------------
DROP TABLE IF EXISTS `advert`;
CREATE TABLE `advert`  (
  `aId` int(0) NOT NULL AUTO_INCREMENT,
  `adContent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '广告内容',
  `adPicUrl` varchar(590) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '广告图片url',
  `byGid` int(0) NULL DEFAULT NULL COMMENT '来自的图片id',
  PRIMARY KEY (`aId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advert
-- ----------------------------
INSERT INTO `advert` VALUES (1, '佳能单反相机', 'https://imgcps.jd.com/img-cubic/creative_server_cia/v2/2000366/60949595133/FocusFullshop/CkRqZnMvdDEvMTkxMTYwLzI3LzEyMjkzLzY1MDIxMC82MGU2OWJlN0U5YWZjOWExNS82ZGRjYjNlZGMzZjYxZDk1LnBuZxIJNC10eV8wXzU1MAI47ot6QhYKEuS9s-iDveWNleWPjeebuOacuhAAQhIKDua7oTU5OTnlh48xMjAwEAFCEAoM56uL5Y2z5oqi6LStEAJCCgoG5Yqb6I2QEAdY_YeEh-MB/cr/s/q.jpg', 1);
INSERT INTO `advert` VALUES (2, '蒙牛纯甄', 'https://imgcps.jd.com/ling4/1975749/6JCl5YW75aW25ZOB5aW954mp/54iG5qy-55u06ZmN/p-5bd8253082acdd181d02fa71/cc8d8723/cr/s/q.jpg', 3);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `cartId` int(0) NOT NULL AUTO_INCREMENT,
  `userId` int(0) NULL DEFAULT NULL,
  `goodsId` int(0) NULL DEFAULT NULL,
  `num` int(0) NULL DEFAULT NULL,
  `sumprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价',
  PRIMARY KEY (`cartId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES (16, 1, 18, 1, 2149.00);
INSERT INTO `cart` VALUES (17, 1, 19, 1, 4729.00);
INSERT INTO `cart` VALUES (18, 1, 1, 1, 6898.00);
INSERT INTO `cart` VALUES (19, 1, 2, 1, 6568.00);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `cId` int(0) NOT NULL AUTO_INCREMENT,
  `userId` int(0) NULL DEFAULT NULL,
  `byGoodsId` int(0) NULL DEFAULT NULL,
  `nickName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '昵称',
  `picUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像url',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '评论内容',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '时间戳',
  PRIMARY KEY (`cId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (2, 1, 2, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '66666', '2021-12-21 21:21:00');
INSERT INTO `comment` VALUES (3, 1, 7, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '66', '2021-12-21 22:32:09');
INSERT INTO `comment` VALUES (4, 1, 18, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '塞尔达yyds', '2021-12-23 15:09:47');
INSERT INTO `comment` VALUES (5, 1, 19, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '只狼!!!', '2021-12-23 15:10:02');
INSERT INTO `comment` VALUES (6, 1, 6, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '666', '2021-12-23 20:22:29');
INSERT INTO `comment` VALUES (8, 1, 3, 'admin', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '俺只喝特仑苏', '2021-12-24 13:18:34');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `gid` int(0) NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `gprice` decimal(10, 2) NULL DEFAULT NULL,
  `pthumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `gdesc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品详情描述',
  PRIMARY KEY (`gid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (1, '佳能200d二代', 6898.00, 'https://img.alicdn.com/imgextra/i1/2205586545403/O1CN01yMknLQ1pmceZSkh8t_!!2205586545403.jpg_430x430q90.jpg', '佳能200d二代 2代 入门级单反相机 vlog便携家用迷你单反数码照相机 白色200DII EF-S18-55套机 套餐一【入门配置 再送599元大礼包】');
INSERT INTO `goods` VALUES (2, '华为mate40pro', 6568.00, 'https://img.alicdn.com/bao/uploaded/i2/268451883/O1CN01nbQf3X1PmSS5E4bG4_!!268451883.jpg', '华为mate40pro 5G手机【支持鸿蒙HarmonyOs】 秘银色 8+256G全网通（4G有充）');
INSERT INTO `goods` VALUES (3, '特仑苏', 89.90, 'https://img.alicdn.com/bao/uploaded/i4/3718861889/O1CN01f5vksZ1PpCrqP1o8T_!!3718861889.jpg', '蒙牛 特仑苏 纯牛奶250ml*16每100ml含3.6g优质蛋白质');
INSERT INTO `goods` VALUES (4, 'FT GUOGE外套', 664.00, 'https://img.alicdn.com/bao/uploaded/i3/3361684045/O1CN01R2av5c1fkefPDi6lN_!!3361684045.jpg', 'FT GUOGE墨绿色双面呢大衣女2021秋冬新款高端复古双排扣毛呢外套');
INSERT INTO `goods` VALUES (5, '菜刀竹虎座竹H刀架', 45.00, 'https://gd3.alicdn.com/imgextra/i1/89193627/O1CN01FkCQJN1cfDCpbSMqr_!!89193627.jpg_400x400.jpg', '菜刀竹虎座竹H刀架刀插百货家用收纳防霉创意一体多功能刀具新款');
INSERT INTO `goods` VALUES (6, '马登工装', 239.90, 'https://gd3.alicdn.com/imgextra/i2/24780476/O1CN01eUq3UK1FO3Bah9sWe_!!24780476.jpg', '马登工装 美式空军MA1飞行员夹克军绿色冬季加厚棒球棉服男潮衣袄');
INSERT INTO `goods` VALUES (7, '山雾花野休闲鞋', 599.00, 'https://img.alicdn.com/imgextra/i3/3064582122/O1CN01WcdMG01RXuw5pgvXo_!!0-item_pic.jpg_430x430q90.jpg', '山雾花野休闲鞋帆布情侣运动鞋男女潮流透气篮球鞋');
INSERT INTO `goods` VALUES (8, '日系多口袋工装夹克', 208.00, 'https://gd1.alicdn.com/imgextra/i1/282443726/O1CN01IM8Mea1dOYRaGC9hx_!!282443726.jpg_400x400.jpg', '嘿马七作日系多口袋工装夹克男春秋季冲锋衣休闲外套美式户外潮牌');
INSERT INTO `goods` VALUES (9, 'F87机械键盘', 155.00, 'https://gd1.alicdn.com/imgextra/i1/185035303/O1CN01wAUBsn1p2ozkk53Z6_!!185035303.jpg_400x400.jpg', '凡酷三代F87机械键盘USB键线分离白光高特插拔青茶黑红轴吸磁上盖');
INSERT INTO `goods` VALUES (10, 'EDIFIER/漫步者', 339.00, 'https://img.alicdn.com/imgextra/i2/346884669/O1CN01exXRmx1kMRvpyFVrw_!!346884669.jpg_430x430q90.jpg', 'EDIFIER/漫步者 LolliPods plus真无线蓝牙运动耳机跑步防尘防水');
INSERT INTO `goods` VALUES (18, '任天堂Switch', 2149.00, 'https://gd4.alicdn.com/imgextra/i4/27013441/O1CN01Djl9bc1bI1ZT5gk0l_!!27013441.jpg', '任天堂（Nintendo）Switch NS掌上游戏机 红蓝手柄 长续航 日版 一机多玩随时尽兴 无线互连共享欢乐');
INSERT INTO `goods` VALUES (19, '索尼PS5', 4729.00, 'https://gd3.alicdn.com/imgextra/i3/493885388/O1CN01BYzHgZ1pfkbrknDLo_!!493885388.jpg', '索尼国行 PS5主机 PlayStation 电视游戏机 蓝光8K 港版日版');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `orderId` int(0) NOT NULL AUTO_INCREMENT,
  `userId` int(0) NULL DEFAULT NULL,
  `orderName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `cartList` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '购物车列表（存储多个购物车id）',
  `orderTime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单时间戳',
  `amount` double NULL DEFAULT NULL COMMENT '订单金额',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`orderId`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (9, 1, '佳能200d二代', '11', '20211221210931231', 13796, '0');
INSERT INTO `orders` VALUES (10, 1, '华为mate40pro,日系多口袋工装夹克,山雾花野休闲鞋', '12,13,14', '20211221212120003', 13943, '0');
INSERT INTO `orders` VALUES (11, 1, '山雾花野休闲鞋,华为mate40pro', '15,16', '20211221223222265', 7766, '0');
INSERT INTO `orders` VALUES (12, 1, '华为mate40pro,佳能200d二代', '9,10', '20211223150933920', 13466, '0');
INSERT INTO `orders` VALUES (14, 1, '特仑苏,EDIFIER/漫步者,任天堂Switch,索尼PS5', '12,13,14,15', '20211224131917652', 7396.8, '0');

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `descp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商家描述',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商家网址',
  `byGoods` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES (1, '佳能旗舰店', '佳能200d二代等办公用品', 'https://canondayin.tmall.com/?spm=a220o.1000855.1997427721.d4918089.3ddd3049uxNEaH', 1);
INSERT INTO `store` VALUES (2, '华为官方旗舰店', '华为mate40pro等电子产品', 'https://list.tmall.com/search_shopitem.htm?spm=a220m.1000858.1000725.2.5bdf766aJ2I2RE&user_id=2838892713&from=_1_&stype=search', 2);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `picUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像图片地址',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin123', 'https://images.cnblogs.com/cnblogs_com/blogs/715063/galleries/2053375/t_211029104329_zk.jpg?a=1635936617445', '北京市', '15628301039', '2662354939@qq.com');
INSERT INTO `user` VALUES (2, 'abcdd', 'root', 'https://i0.hdslb.com/bfs/face/c763774881473c14ce1502689cc7b29bb46eaf04.jpg@256w_256h_1o.webp', '北京市', '15628301035', '2662354929@qq.com');
INSERT INTO `user` VALUES (9, 'admin324', '111', '32423', '324234', '2342', '234');
INSERT INTO `user` VALUES (10, '423423', '111', '234234', '243324', '234324', '2342');
INSERT INTO `user` VALUES (11, 'admin55', '123', '213123', '123', '213', '111');
INSERT INTO `user` VALUES (23, 'user', 'user123', 'https://i2.hdslb.com/bfs/face/a34eddc74f7bffb96f77b37b4f27b793d892863b.jpg@240w_240h_1c_1s.webp', '北京市', '15628301033', '2662354939@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
